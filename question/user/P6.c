/* Copyright (C) 2017 Daniel Page <csdsp@bristol.ac.uk>
 *
 * Use of this source code is restricted per the CC BY-NC-ND license, a copy of 
 * which can be found via http://creativecommons.org (and should be included as 
 * LICENSE.txt within the associated archive or repository).
 */

#include "P6.h"

extern void main_phil();

/* P6 addresses the dining philosophers problem:
 * - Single pipe has been chosen
 * - Current method used is not great because
 *      1) Am using one pipe through which both parent and child are reading and writing
 *      2) Should have two pipes
 *      3) One consequence of current implementation is that there is lots of idle time as forks are not removed and then reassigned, both operations are done at once meaning some processes have to wait another go for their forks
 */
void main_P6() {
	
	write(STDOUT_FILENO, "DP", 2);
	READY_TO_TRANSMIT = false;

	// set file descriptor values
	fd[0] = PIPE_READ_END;
	fd[1] = PIPE_WRITE_END;

	//initialise all fork values to free
	//initialise all phils to thinking
	char forkVals[noPhils];
	char philState[noPhils];
	int eatDistribution[noPhils];
	for (int i = 0; i < noPhils; i++) {
		forkVals[i] = forkFree;
		philState[i] = THINKING;
	}

	// open pipe needed to communicate with children philosophers
	int openPipe = pipe(fd);
	if (openPipe == 0) { write(STDOUT_FILENO, "pipe worked", 11); }
	else { write(STDOUT_FILENO, "pipe failed", 11); }

	// use fork to create children philosophers
	// then use pipe to communicate which child number they are
	for (int i = 0; i < noPhils; i++) {
		if (fork() == 0) { //child process
			exec(main_phil);
		}
		else {
			// communicate to child down pipe which child they are
			int childNo = i;
			char* childNoChar;
			itoa(childNoChar, childNo);
			write(fd[1], childNoChar, 2);
		}
	}
	//yield to allow children to read out their child type
	// use flag to show that initilisation has finished and dining philosopher problem can begin
	READY_TO_TRANSMIT = true;
	yield();

	while (1) {
		for (int i = 0; i < noPhils; i++) {
			char* forksFree = "0";
			char* forksUsed = "1";
			char readInfo[2];

			read(fd[0], readInfo, 2);
			int x = atoi(readInfo);

			if (philState[x] == THINKING) {
				if (forkVals[x] == forkFree && forkVals[(x + 1) % noPhils] == forkFree) {
					forkVals[x] = forkUsed;
					forkVals[(x + 1) % noPhils] = forkUsed;
					philState[x] = EATING;
					//write to child that it has the forks and may eat
					write(fd[1], forksFree, 2);
				}
				else { write(fd[1], forksUsed, 2); }
			}
			else { // take forks back away
				forkVals[x] = forkFree;
				forkVals[(x + 1) % noPhils] = forkFree;
				philState[x] = THINKING;
				write(fd[1], forksUsed, 2);
				//write(STDOUT_FILENO, "write to child", 15);
			}
		}

		//Prints out to UART0 the status of each child and how many times they have eaten to check long term distribution of forks
		for (int i = 0; i < noPhils; i++) {
			char childNo[2];
			itoa(childNo, i);
			if (philState[i] == EATING) {
				eatDistribution[i]++;
				write(STDOUT_FILENO, childNo, 2);
				write(STDOUT_FILENO, ": currently EATING,",20);
				write(STDOUT_FILENO, "has eaten" , 10);
				char nom[2];
				itoa(nom, eatDistribution[i]);
				write(STDOUT_FILENO, nom, 2);
				write(STDOUT_FILENO, "times.", 7);
			}
			else {
				write(STDOUT_FILENO, childNo, 2);
				write(STDOUT_FILENO, ": currently THINKING", 21);
				write(STDOUT_FILENO, "has eaten", 10);
				char nom[2];
				itoa(nom, eatDistribution[i]);
				write(STDOUT_FILENO, nom, 2);
				write(STDOUT_FILENO, "times.", 7);
			}
		}
		yield();
	}

	exit(EXIT_SUCCESS);
}