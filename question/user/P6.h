/* Copyright (C) 2017 Daniel Page <csdsp@bristol.ac.uk>
 *
 * Use of this source code is restricted per the CC BY-NC-ND license, a copy of 
 * which can be found via http://creativecommons.org (and should be included as 
 * LICENSE.txt within the associated archive or repository).
 */

#ifndef __P6_H
#define __P6_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "libc.h"

//define values used in dining philosophers problem
int noPhils = 16; //number of philosophers
char forkFree = '0';
char forkUsed = '1';
int fd[2];

#define THINKING (0)
#define EATING   (1)

#endif
