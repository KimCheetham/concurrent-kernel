/* Copyright (C) 2017 Daniel Page <csdsp@bristol.ac.uk>
 *
 * Use of this source code is restricted per the CC BY-NC-ND license, a copy of 
 * which can be found via http://creativecommons.org (and should be included as 
 * LICENSE.txt within the associated archive or repository).
 */

#include "phil.h"

void main_phil() {
	int fd[2];
	fd[0] = PIPE_READ_END;
	fd[1] = PIPE_WRITE_END;
	int childNoInt;
	int philStatus = THINKING;
	char* forksFree = "0";
	char* forksUsed = "1";

	// Child reads from pipe a number so they know which child they are
	char childNo[2];
	read(fd[0], childNo, 2);
	write(STDOUT_FILENO, " I am philosopher number", 25);
	write(STDOUT_FILENO, childNo, 2);
	childNoInt = atoi(childNo);

	while (1) {
		if(READY_TO_TRANSMIT == true) {
			char childNoChar[2];
			itoa(childNoChar, childNoInt);
			//writing down pipe serves as both a request for forks and to say philosopher is finished eating 
			write(fd[1], childNoChar, 2);
			//yield needed so that parent can deal with request
			yield();

			// Read the response from the parent from down the pipe
			char pipeInfo[2];
			read(fd[0], pipeInfo, 2);
			int x = atoi(pipeInfo);
			if(x == 0) {
				//eat with the forks
				write(STDOUT_FILENO, "nom", 4);
				philStatus = EATING;
			}
			else {
				write(STDOUT_FILENO, "think", 6);
				philStatus == THINKING;
			}
		}
		else {
			//yield because parent is not able to deal with request
			yield();
		}
	}
	exit( EXIT_SUCCESS );
}
