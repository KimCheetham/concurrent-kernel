/* Copyright (C) 2017 Daniel Page <csdsp@bristol.ac.uk>
 *
 * Use of this source code is restricted per the CC BY-NC-ND license, a copy of 
 * which can be found via http://creativecommons.org (and should be included as 
 * LICENSE.txt within the associated archive or repository).
 */

#include "hilevel.h"

/* Initialise all tables needed in kernel:
 * 1) Process tab to store PCBs
 * 2) Free process tab (used to determine which processes in the procTab are available to be used
 *	  (will remove this table and find a way to use values in procTab to determine if it is free (i.e. not READY or EXECUTING) if I have time
 * 3) Open file tab contains an instance for every time a file is opened, is pointed to via a process specific file descriptor tab (found in the PCB)
 * 4) Inode tab which contains one instance for each file in use, is pointed to via the openFileTab
 */
pcb_t procTab[MAX_PROCS];
int freeProcs[MAX_PROCS];
pcb_t* executing = NULL;
file_t openFileTab[MAX_FILE];
inode_t inodeTab[MAX_INODES];

/* Dispatch function:
 * 1) Preserves the execution context of the previously executing program
 * 2) Restores the execution context of the next executing program
 * 3) Prints to UART0 the process change that has taken place
 * 4) Updates the currently executing program to the next program
 */
void dispatch(ctx_t* ctx, pcb_t* prev, pcb_t* next) {
	char prev_pid = '?';
	char next_pid = '?';
	char prev_pid_2 = '?';
	char next_pid_2 = '?';

	if (NULL != prev) {
		memcpy(&prev->ctx, ctx, sizeof(ctx_t));
		int intPrev = prev->pid;
		if (intPrev > 9) {
			prev_pid = '0' + intPrev / 10;
			prev_pid_2 = '0' + intPrev % 10;
		}
		else { prev_pid = '0' + intPrev; }
	}

	if (NULL != next) {
		memcpy(ctx, &next->ctx, sizeof(ctx_t)); 
		int intNext = next->pid;
		if (intNext > 9) {
			next_pid = '0' + intNext / 10;
			next_pid_2 = '0' + intNext % 10;
		}
		else { next_pid = '0' + intNext; }
	}

	PL011_putc(UART0, '[', true);
	PL011_putc(UART0, prev_pid, true);
	if (prev_pid_2 != '?') { PL011_putc(UART0, prev_pid_2, true); }
	PL011_putc(UART0, '-', true);
	PL011_putc(UART0, '>', true);
	PL011_putc(UART0, next_pid, true);
	if (next_pid_2 != '?') { PL011_putc(UART0, next_pid_2, true); }
	PL011_putc(UART0, ']', true);

	executing = next;
	return;
}

/* Scheduling decisions based on:
 * -  static initial priority set when process is initialised (will implement nice system call if time)
 * -  dynamically increasing priority that is iterated every time the process has to wait to be executed
 * 1) Reset the dynamic priority of the currently executing program then revert its status to READY (unless terminated)
 * 2) Increment dynamic priority of all other processes
 * 3) Set the next executing program to be the one with the highest combined priority
 * 4) Update status of next program to EXECUTING and call dispatch to make the context switch
 */
void schedule(ctx_t* ctx) {
	pcb_t* current;
	pcb_t* next;
	for (int i = 0; i < MAX_PROCS; i++) {
		if (executing->pid == procTab[i].pid) {
			current = &procTab[i];
			procTab[i].dynoPr = 0;
			if(procTab[i].status != STATUS_TERMINATED) { procTab[i].status = STATUS_READY; }
		}
		else {
			if (procTab[i].status == STATUS_READY) { procTab[i].dynoPr = procTab[i].dynoPr + 1; }
		}
	}
	int priority = 0;
	for (int i = 0; i < MAX_PROCS; i++) {
		if ((procTab[i].dynoPr + procTab[i].statPr > priority) && procTab[i].status != STATUS_TERMINATED) {
			priority = procTab[i].dynoPr + procTab[i].statPr;
			next = &procTab[i];			
		}
	}
	next->status = STATUS_EXECUTING;
	dispatch(ctx, current, next);
	return;
}

extern void		main_console();
extern uint32_t tos_console;
extern uint32_t tos_usr;

// returns next available pcb i.e. the first one with 0 in the freeProcs table)
int getNextFree() {
	for (int i = 0; i < MAX_PROCS; i++) {
		if (freeProcs[i] == 0) { return i; }
	}
	return -1;
}

//returns next available inode i.e. the one with no name
int getNextInode() {
	for (int i = 0; i < MAX_INODES; i++) {
		if (inodeTab[i].type == INVALID_TYPE) { return i; }
	}
	return -1;
}

//returns the next available open file slot by checking whether it has a pointer to an inode
int getNextOpenFile() {
	for (int i = 0; i < MAX_FILE; i++) {
		if (openFileTab[i].mode == INVALID_MODE) { return i; }
	}
	return -1;
}

// returns the length of info from file by detecting when there is a null character signifying the end of the transmission of that piece of information
int getBlockContentsLength(uint8_t* block, int startPoint, int blockLength) {
	for (int i = startPoint; i < blockLength; i++) {
		if (block[i] == 0x00) { return i - startPoint; }
	}
	return blockLength;
}

void hilevel_handler_rst(ctx_t* ctx) {
	/* Timer interrupt handling:
	* - Timer configured st. it raises a (periodic) interrupt for each timer tick
	* - GIC configured st. the selected interrupts are forwarded to the processor via the IRQ interrupt signal, then
	* - IRQ interrupts are enabled.
	*/
	TIMER0->Timer1Load = 0x00100000; // select period = 2^20 ticks ~= 1 sec
	TIMER0->Timer1Ctrl = 0x00000002; // select 32-bit timer
	TIMER0->Timer1Ctrl |= 0x00000040; // select periodic timer
	TIMER0->Timer1Ctrl |= 0x00000020; // enable timer interrupt
	TIMER0->Timer1Ctrl |= 0x00000080; // enable timer
	GICC0->PMR = 0x000000F0; // unmask all interrupts
	GICD0->ISENABLER1 |= 0x00000010; // enable timer interrupt
	GICC0->CTLR = 0x00000001; // enable GIC interface
	GICD0->CTLR = 0x00000001; // enable GIC distributor

	int_enable_irq();

	// Initialise the first process tab entry to be the console
	memset(&procTab[0], 0, sizeof(pcb_t));
	procTab[0].pid = 1;
	procTab[0].status = STATUS_READY;
	procTab[0].tos = (uint32_t)(&tos_console);
	procTab[0].ctx.cpsr = 0x50;
	procTab[0].ctx.pc = (uint32_t)(&main_console);
	procTab[0].ctx.sp = procTab[0].tos;
	procTab[0].statPr = 3;
	freeProcs[0] = 1;

	//statically allocate all memory needed for future user processes not including console
	for (int i = 1; i < MAX_PROCS; i++) {
		memset(&procTab[i], 0, sizeof(pcb_t));
		procTab[i].pid = -1;
		procTab[i].status = STATUS_INVALID;
		procTab[i].tos = (uint32_t)(&tos_usr - ((i - 1) * 0x00000500));
		procTab[i].ctx.cpsr = 0x50;
		procTab[i].ctx.sp = procTab[i].tos;
	}

	dispatch(ctx, NULL, &procTab[0]);
	return;
}

// Handle timer interrupts
void hilevel_handler_irq(ctx_t* ctx) {
	uint32_t id = GICC0->IAR; // read  the interrupt identifier so we know the source.
	if (id == GIC_SOURCE_TIMER0) { // handle the interrupt, then clear (or reset) the source.
		TIMER0->Timer1IntClr = 0x01; schedule(ctx);
	}
	GICC0->EOIR = id; // write the interrupt identifier to signal we're done.
	return;
}

// Handle all system calls 
void hilevel_handler_svc(ctx_t* ctx, uint32_t id) {
	switch (id) {
		case 0x00: { // 0x00 => yield()
			schedule(ctx); break;
		}
		case 0x01: { // 0x01 => write( fd, x, n )
			int   fd = (int)(ctx->gpr[0]);
			char*  x = (char*)(ctx->gpr[1]);
			int    n = (int)(ctx->gpr[2]);
			if (fd == 1) { // STDOUT writes to UART0, returns number of characters written
				for (int i = 0; i < n; i++) { PL011_putc(UART0, *x++, true); }
				ctx->gpr[0] = n;
			}
			else if (fd == 4) { // Writes to pipe
				/* 1) Uses location pointer in inode (accessed through file descriptors) to access pipe on heap
				 * 2) Maintains a count of the write offset which is reset once the end of the buffer is reached
				 * 3) Returns number of characters written to pipe
				 */
				file_t* pipe = executing->fileDescTab[fd];
				inode_t* pipeInode = pipe->ptrInode;
				for (int i = 0; i < n; i++) {
					pipeInode->location[pipe->writeOffset] = x[i];
					pipe->writeOffset++;
				}
				if (pipe->writeOffset >= BUFFER_SIZE - 1) {
					PL011_putc(UART0, 'Y', true);
					pipe->writeOffset = 0;
				}
				ctx->gpr[0] = n;
			}
			else if (fd > 4) { // Writes to file
				/* 1) Inode accessed through file descriptor in order to access file size and location on disk
				2) Make request to disk to get block length
				3) Check the mode to see if the file is allowed to write
				4) Determines the number of blocks needed to contain the information being written
				5) Writes information to disk and then returns number of characters written
				*/
				file_t* file = executing->fileDescTab[fd]; //get file given by fd
				inode_t* inode = file->ptrInode; //get inode for file
				uint32_t diskAddress = inode->diskLoc; //get disk address
				int blockLen = disk_get_block_len(); //make request to know disk block length
				int offset = 0;
				int blockNum = n / blockLen + 1;
				for (int i = 0; i < n; i++) {
					inode->location[i] = x[i];
				}
				if (file->mode == WRITE_ONLY) {
					for (int i = 0; i < blockNum; i++) {
						char block[16];
						for (int j = 0; j < blockLen; j++) {
							block[j] = inode->location[offset];
							offset++;
						}
						disk_wr(diskAddress + i, block, blockLen);
					}
					file->writeOffset = n;
					ctx->gpr[0] = n;
				}
				else {
					ctx->gpr[0] = 0; // no characters written as wrong mode
				}
				
			}
			else { ctx->gpr[0] = 0; }
			break;
		}
		case 0x02: { // 0x02 => read( fd, x, n)
			int   fd = (int)(ctx->gpr[0]);
			char*  x = (char*)(ctx->gpr[1]);
			int    n = (int)(ctx->gpr[2]);
			if (fd == 0) { // STDIN
				ctx->gpr[0] = 0;
			}
			else if (fd == 3) { // Read from pipe
				/* 1) Uses location pointer in inode (accessed through file descriptors) to access pipe on heap
				 * 2) Maintains a count of the read offset which is reset once the end of the buffer is reached
				 * 3) Returns number of characters read from pipe
				 */
				file_t* pipe = executing->fileDescTab[fd];
				inode_t* pipeInode = pipe->ptrInode;
				for (int i = 0; i < n; i++) {
					x[i] = pipeInode->location[pipe->readOffset];
					pipe->readOffset++;
				}
				if(pipe->readOffset >= BUFFER_SIZE - 1) {
					PL011_putc(UART0, 'X', true);
					pipe->readOffset = 0;
				}
				ctx->gpr[0] = n;
			}
			else if (fd > 4) { //read from file
				/* 1) Get disk location, heap location and size of disk from inode
				 * 2) If n=0, the entire file contents needs to be read so ake request from disk to read the number of blocks given by size
				 * 2) Otherwise read the number of blocks required to read n characters
				 * 3) Read the info found in these blocks onto the heap space, maintaining an offset and detecting the end of the file using the 0x00 character
				 * 4) Read from the heap space to x and return the number of characters read
				 */
				file_t* file = executing->fileDescTab[fd];
				inode_t* inode = file->ptrInode;
				uint32_t diskAddress = inode->diskLoc;
				int blockLen = disk_get_block_len();
				int offset = 0;
				if (file->mode == READ_ONLY) {
					if (n == 0) { //request to read entire disk
						int charsRead = 0;
						bool endOfFile = false;
						for (int i = 0; i < inode->size; i++) {
							char block[16];
							disk_rd(diskAddress + i, block, blockLen);
							for (int j = 0; j < blockLen; j++) {
								if (block[j] == 0x00) { endOfFile = true; }
								else { inode->location[offset] = block[j]; }
								offset++;
							}
							if (endOfFile == true) { break; }
						}
						for (int i = 0; i < offset; i++) {
							x[i] = inode->location[i];
							//PL011_putc(UART0, inode->location[i], true); //debugging output to check contents of file
							charsRead++;
						}
						ctx->gpr[0] = charsRead;
						break;
					}
					else { //request specific number of characters to be read
						char readInfo[n];
						int blockNum = n / blockLen + 1;
						int charsRead = 0;
						bool endOfFile = false;
						for (int i = 0; i < blockNum; i++) {
							char block[16];
							disk_rd(diskAddress + 1, block, blockLen);
							for (int j = 0; j < blockLen; j++) {
								if (block[j] == 0x00) { endOfFile = true; }
								else { inode->location[offset] = block[j]; }
								offset++;
							}
							if (endOfFile == true) { break; }
						}
						for (int i = 0; i < offset; i++) {
							charsRead++;
							if (charsRead <= n) {
								x[i] = inode->location[i];
							}
						}
						file->readOffset = charsRead; //set read offset
						ctx->gpr[0] = charsRead;
						break;
					}
				}
				else {
					ctx->gpr[0] = 0; // Return that no characters were read as wrong mode
					break;
				}
			}
			else { ctx->gpr[0] = 0; }
			break;
		}
		case 0x03: { // 0x03 => fork()
			/* 1) Copy the context of the parent into the next available procTab space
			 * 2) Copy contents of file descriptor table into child so that it maintains the same open files
			 * 3) Set return value for child to be 0
			 * 4) Copy the contents of the stack
			 */
			int nextFree = getNextFree();
			pcb_t* parent = executing;
			pcb_t* child = &procTab[nextFree];
			uint32_t sizeOfStack = (parent->tos - ctx->sp);
			memcpy(&procTab[nextFree].ctx, ctx, sizeof(ctx_t));
			child->pid = nextFree + 1;
			child->status = STATUS_READY;
			child->ctx.sp = child->tos - sizeOfStack;
			child->ctx.pc = ctx->pc;
			child->statPr = parent->statPr;
			for (int i = 0; i < MAX_FILE; i++) {
				child->fileDescTab[i] = parent->fileDescTab[i];
				// Increment open count if file is not a pipe
				file_t* file = child->fileDescTab[i];
				inode_t* inode = file->ptrInode;
				if (inode->type != PIPE && inode->type != INVALID_MODE) {
					inode->openCount++;
				}
			}
			child->ctx.gpr[0] = 0;
			memcpy((void*)(child->tos - sizeOfStack), (void*)(parent->tos - sizeOfStack), sizeOfStack);
			freeProcs[nextFree] = 1;
			break;
		}
		case 0x04: { // 0x04 => exit(x)
			/* 1) Change the status of the process to TERMINATED
			 * 2) Change the respective slot to 'free' in the freeProcs table
			 * 3) Call schedule
			 */
			for (int i = 0; i < MAX_PROCS; i++) {
				if (executing->pid == procTab[i].pid) {
					procTab[i].status = STATUS_TERMINATED;
					freeProcs[i] = 0;
				}
			}
			schedule(ctx);
			break;
		}
		case 0x05: { // 0x05 => exec(const void* x)
			// Change the stack pointer to be the program needed to be executed
			uint32_t x = (uint32_t)(ctx->gpr[0]);
			ctx->pc = x;
			break;
		}
		case 0x06: { // 0x06 => kill(int pid, int x)
			/* 1) Change the status of the process to TERMINATED
			 * 2) Change the respective slot to 'free' in the freeProcs table
			 * 3) Call schedule
			 */
			pid_t x = (pid_t)(ctx->gpr[0]);
			for (int i = 0; i < MAX_PROCS; i++) {
				if (x == procTab[i].pid) {
					procTab[i].status = STATUS_TERMINATED;
					freeProcs[i] = 0;
				}
			}
			schedule(ctx);
			break;
		}
		case 0x07: { // 0x07 => nice(int pid, int x)
			// TO DO: Implement so that a process can make a system call to change its own priority
			int pid = (int)(ctx->gpr[0]);
			int x = (int)(ctx->gpr[0]);
		}
		case 0x08: { // 0x08 => pipe(int fd[])
			/* 1) Dynamically allocate space for pipe on the heap
			 * 2) Add pipe to inodeTab
			 * 3) Add pipe to openFileTab
			 * 4) Create links to these tables through the executing processes file descriptors
			 */
			int* fd = (int*)(ctx->gpr[0]);
			char* pipe;
			pipe = (char*)malloc(BUFFER_SIZE);

			int nextInode = getNextInode();
			//TO DO: naming of pipes so that they are uniquely named in inodeTab if there was more than one created
			inodeTab[nextInode].name = "pipe";
			inodeTab[nextInode].type = PIPE;
			inodeTab[nextInode].openCount = 1;
			inodeTab[nextInode].size = BUFFER_SIZE;
			inodeTab[nextInode].location = pipe;
			
			int nextFile = getNextOpenFile();
			openFileTab[nextFile].mode = PIPE_RW;
			openFileTab[nextFile].ptrInode = &inodeTab[nextInode];
			openFileTab[nextFile].readOffset = 0;
			openFileTab[nextFile].writeOffset = 0;
			
			executing->fileDescTab[fd[0]] = &openFileTab[nextFile];
			executing->fileDescTab[fd[1]] = &openFileTab[nextFile];
			
			ctx->gpr[0] = 0;
			break;
		}
		case 0x09: { // open file
			/* 1) Check to see if the file named is already open and therefore present in the inodeTab
			 * 2) If not, search the first 20 blocks of the disk (where a simple single level directory is found) for an inode with the same name
			 * 3) If such an inode exists, load the information found in it to the inodeTab
			 * 4) Create a new openFileTab instance of the file being opened
			 * 5) Create and return a file descriptor that points to the openFileTab
			 */
			char* fileName = (char*)(ctx->gpr[0]);
			mode fileMode = (mode)(ctx->gpr[1]);
			inode_t* file;
			bool alreadyOpen = false;
			for (int i = 0; i < MAX_INODES; i++) { //check to see if the file already exists in the inode table
				if (0 == strcmp(fileName, inodeTab[i].name)) {
					file = &inodeTab[i];
					inodeTab[i].openCount++;
					alreadyOpen = true;
					break;
				}
			}
			if (alreadyOpen == false) {
				int blockLen = disk_get_block_len();
				int blockPosition = 0;
				bool fileFound = true;
				uint8_t inodeInfo[blockLen];
				for (int i = 0; i < MAX_FILE; i++) {
					disk_rd(i, inodeInfo, blockLen);
					int diskNameLength = getBlockContentsLength(inodeInfo, blockPosition, blockLen);
					int inputFileLength = strlen(fileName);
					if (diskNameLength > inputFileLength || diskNameLength < inputFileLength) {
						fileFound = false;
					}
					else {
						for (int j = blockPosition; j < diskNameLength; j++) {
							if (inodeInfo[j] != fileName[j - blockPosition]) {
								fileFound = false;
								break;
							}
							else {
								fileFound = true;
							}
						}
						if (fileFound == true) {
							blockPosition = blockPosition + diskNameLength + 1;
							break;
						}
					}

				}
				if (fileFound == false) {
					ctx->gpr[0] = 0; //return that file does not exist
					break;
				}

				//add file to inode table
				int nextInode = getNextInode();
				inodeTab[nextInode].name = fileName;

				int diskTypeLength = getBlockContentsLength(inodeInfo, blockPosition, blockLen);
				if(inodeInfo[blockPosition] == 't') { inodeTab[nextInode].type = TEXT; }
				else if (inodeInfo[blockPosition] == 'e') { inodeTab[nextInode].type = EXCECUTABLE; }
				blockPosition = blockPosition + diskTypeLength + 1;
				
				inodeTab[nextInode].openCount = 1;
				
				int diskSizeLength = getBlockContentsLength(inodeInfo, blockPosition, blockLen);
				int size = 0;
				for (int i = blockPosition; i < blockPosition + diskSizeLength; i++) {
					int sizeDigit = inodeInfo[i] - 0x30;
					size = size * 10;
					size = size + sizeDigit;
				}
				inodeTab[nextInode].size = size;
				blockPosition = blockPosition + diskSizeLength + 1;
				
				int diskLocationLength = getBlockContentsLength(inodeInfo, blockPosition, blockLen);
				int loc = 0;
				for (int i = blockPosition; i < blockPosition + diskLocationLength; i++) {
					int locDigit = inodeInfo[i] - 0x30;
					loc = loc * 10;
					loc = loc + locDigit;
				}
				inodeTab[nextInode].diskLoc = loc;
				file = &inodeTab[nextInode];	

				char* heapSpace;
				heapSpace = malloc(size * blockLen);
				inodeTab[nextInode].location = heapSpace;
			}

			// Add instance of file to open file table
			int nextFile = getNextOpenFile();
			openFileTab[nextFile].mode = fileMode;
			openFileTab[nextFile].ptrInode = file;
			openFileTab[nextFile].readOffset = 0;
			openFileTab[nextFile].writeOffset = 0;

			// Create new file descriptor and return it
			int nextFd;
			for (int i = 5; i < MAX_FILE; i++) {
				if (executing->fileDescTab[i] == 0x0) { nextFd = i; break; }
			}
			executing->fileDescTab[nextFd] = &openFileTab[nextFile];
			ctx->gpr[0] = nextFd;
			break;
		}
		case 0x10: { //close file
			/* 1) Decrement open count by 1 and remove inode if open count has become 0
			 * 2) Remove instance of open file from openFileTab and clear file desciptor entry
			 */
			int fd = (int)(ctx->gpr[0]);
			file_t* file = executing->fileDescTab[fd];
			inode_t* inode = file->ptrInode;

			inode->openCount--;
			if (inode->openCount == 0) {
				inode->name = 0x0;
				inode->type = INVALID_TYPE;
				inode->size = 0x0;
				inode->diskLoc = 0x0;
			}

			file->mode = INVALID_MODE;
			file->ptrInode = 0x0;
			file->readOffset = 0;
			file->writeOffset = 0;
			executing->fileDescTab[fd] = 0x0;
			break;
		}
		default: { // 0x?? => unknown/unsupported
			break;
		}
	}
	return;
}
