/* Copyright (C) 2017 Daniel Page <csdsp@bristol.ac.uk>
 *
 * Use of this source code is restricted per the CC BY-NC-ND license, a copy of 
 * which can be found via http://creativecommons.org (and should be included as 
 * LICENSE.txt within the associated archive or repository).
 */

#ifndef __HILEVEL_H
#define __HILEVEL_H

// Include functionality relating to newlib (the standard C library).

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

#include <string.h>

// Include functionality relating to the platform.

#include   "GIC.h"
#include "PL011.h"
#include "SP804.h"
#include "disk.h"

// Include functionality relating to the   kernel.

#include "lolevel.h"
#include     "int.h"

/* The kernel source code is made simpler and more consistent by using
* some human-readable type definitions:
*
* - a type that captures a Process IDentifier (PID), which is really
*   just an integer,
* - an enumerated type that captures the status of a process, e.g.,
*   whether it is currently executing,
* - a type that captures each component of an execution context (i.e.,
*   processor state) in a compatible order wrt. the low-level handler
*   preservation and restoration prologue and epilogue, and
* - a type that captures a process PCB.
*/

//define stuff to do with pipes and IPC
#define BUFFER_SIZE 100

#define MAX_PROCS 20
#define MAX_FILE 20
#define MAX_INODES 10

typedef int pid_t;

//kim code
typedef enum {
	INVALID_MODE,
	READ_ONLY,
	WRITE_ONLY,
	PIPE_RW
} mode;

typedef enum {
	INVALID_TYPE,
	TEXT,
	EXCECUTABLE,
	PIPE
} type;

typedef struct {
	char* name;
	type type;
	int openCount;
	int size;
	char* location;
	uint32_t diskLoc;
} inode_t;

typedef struct {
	mode mode; 
	inode_t* ptrInode;
	int readOffset;
	int writeOffset;
} file_t;

typedef enum {
	STATUS_INVALID,

	STATUS_CREATED,
	STATUS_TERMINATED,

	STATUS_READY,
	STATUS_EXECUTING,
	STATUS_WAITING
} status_t;



typedef struct {
	uint32_t cpsr, pc, gpr[13], sp, lr;
} ctx_t;

typedef struct {
	pid_t    pid; // Process IDentifier (PID)
	status_t status; // current status
	uint32_t tos; // address of Top of Stack (ToS)
	ctx_t    ctx; // execution context
	int		 statPr; // static priority
	int		 dynoPr; // dynamic priority
	file_t*  fileDescTab[MAX_FILE];
} pcb_t;

#endif
